class CfgPatches {
    class uag_radios_tfar_beta {
        units[] = {
            "UAG_Predator_Radio_MTP",
			"UAG_Motherlode_Radio_MTP",
			"UAG_TerminalStyle_Radio_Black"
        };
        weapons[] = {};
        requiredVersion = 1.56;
        requiredAddons[] = {"tfar_core"};
        author = "";
        authors[] = {"Unnamed Arma Group", "Cody (@Zeue)"};
        authorUrl = "https://uagpmc.com";
    };
};

class CfgVehicles {
    class CUP_B_Predator_Radio_MTP;
    class UAG_Predator_Radio_MTP : CUP_B_Predator_Radio_MTP {
		displayName = "UAG Predator Radio (MTP)";
        tf_dialog = "anarc210_radio_dialog";
        tf_dialogUpdate = "[""CH%1""] call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 40000;
    }
	
	class CUP_B_Motherlode_Radio_MTP;
    class UAG_Motherlode_Radio_MTP : CUP_B_Motherlode_Radio_MTP {
		displayName = "UAG Motherlode Radio (MTP)";
        tf_dialog = "anarc210_radio_dialog";
        tf_dialogUpdate = "[""CH%1""] call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 40000;
    }
	
	class CUP_B_UAVTerminal_Black;
	class UAG_TerminalStyle_Radio_Black : CUP_B_UAVTerminal_Black {
		displayName = "UAG Terminal-Style Radio (Black)";
        tf_dialog = "anarc210_radio_dialog";
        tf_dialogUpdate = "[""CH%1""] call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 40000;
	}
}